import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

/**
 * Accepts puzzle details from a file passed as a parameter.
 * Outputs the word location. 
 * 
 * Example:
 * File content:
 * 5x5
 * 
 * H A S D F
 * G E Y B H
 * J K L Z X
 * C V B L N
 * G O O D O
 * 
 * HELLO
 * GOOD
 * BYE
 *
 * Output:
 * HELLO 0:0 4:4
 * GOOD 4:0 4:3
 * BYE 1:3 1:1
 *
 * 
 * @author Alamgir Kamal
 * 
 */
public class AlphabetSoup {

	/**
	 * @author Alamgir Kamal
	 * 
	 * Identifies positon of each cell in the grid.
	 */
	static class Position {
		int row;
		int col;

		/**
		 * Constructor
		 * @param row
		 * @param col
		 */
		Position (int row, int col) {
			this.row = row;
			this.col = col;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + col;
			result = prime * result + row;
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Position other = (Position) obj;
			if (col != other.col)
				return false;
			if (row != other.row)
				return false;
			return true;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#toString()
		 */
		@Override
		public String toString() {
			return row + ":" + col;
		}
	}	

	/**
	 * Search routine.  Uses recursively routine to search for a string in the grid.
	 * @param grid, identified grid
	 * @param position, starting position
	 * @param word, word to search
	 * @param path, accumulated positions of the words until the search is complete.
	 * @return  List of positions.
	 */
	static List<Position> search(String[] grid, Position position, String word, List<Position> path) {

		try {
			// Word is found.  No more characters to search.
			if (word.isEmpty()) {
				return path;
			}

			// Unmatched character encountered.
			if (path.contains(position) || (word.charAt(0) != grid[position.row].charAt(position.col))) {
				return new LinkedList<Position>();
			}

			// Search path.
			List<Position> result = new LinkedList<Position>();

			// temporary placeholder for path	
			List<Position> newPath = new LinkedList<Position>();
			newPath.addAll(path);
			newPath.add(position);

			Position point1;
			Position point2;
			int row;
			int col;
			
			// Search in all directions for second character
			if (path.size() <2) {
				for (int r = -1; r <= 1; r++) {
					for (int c = -1; c <= 1; c++) {
						result.addAll(search(grid, new Position(position.row + r, position.col + c), word.substring(1), newPath));
					}					
				}
			}
			// Search in only in one direction for the rest.
			else {
				point1 = path.get(path.size()-1);
				point2 = path.get(path.size()-2);
				row = point1.row -point2.row;
				col = point1.col - point2.col;
				result.addAll(search(grid, new Position(position.row + row, position.col + col), word.substring(1), newPath));
			}

			return result;
			
		} catch (ArrayIndexOutOfBoundsException e) {
			return new LinkedList<Position>();
		} catch (StringIndexOutOfBoundsException e) {
			return new LinkedList<Position>();
		}
	}	
	
	public static void main(String[] args) throws FileNotFoundException{
		
		// check for agruments
		if (args.length == 0) {
			System.out.println("You must provide input file name.  i.e. ./resources/puzzle-1.txt");
			System.exit(0);
		}
		// Check for the validity of the file.
		else {
			File f = new File(args[0]);
			if (!(f.exists() && !f.isDirectory())) { 
				System.out.println("Unable to find/read the file");
				System.exit(0);
			}
		}
		
		// Read File
		Scanner scanner = new Scanner(new File(args[0]));
		scanner.useDelimiter(System.lineSeparator()+System.lineSeparator());

		String gridSize  = scanner.next();
		String[] grid = scanner.next().split(System.lineSeparator());
		String[] words = scanner.next().split(System.lineSeparator());

		scanner.close();
	
		//For simplification purpose remove spaces for each string in the grid.
		for (int i =0; i<grid.length; i++) {
			grid[i] = grid[i].replaceAll(" ", "");
		}
		
		List<Position> wordLocationMap = new LinkedList<Position>(); 

		for (int i = 0; i < words.length; i++) {
			// System.out.println(words[i].trim().toUpperCase());
			String word = words[i].trim().toUpperCase();
			for (int row = 0; row < grid.length; row++) {
				for (int col = 0; col < grid[row].length(); col++) {
					wordLocationMap = search(grid, new Position(row, col), word, new LinkedList<Position>());						
					
					if (wordLocationMap.size() > 0) {						
						System.out.print(word + " ");
						System.out.print(wordLocationMap.get(0) + " " + wordLocationMap.get(wordLocationMap.size()-1));						
						System.out.println();
					}
				}
			}
		}
	}
}
